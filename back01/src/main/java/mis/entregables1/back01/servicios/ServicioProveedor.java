package mis.entregables1.back01.servicios;

import mis.entregables1.back01.modelo.Proveedor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioProveedor {
    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Proveedor> proveedor = new ConcurrentHashMap<>();

    public long agregar(Proveedor p){
        p.id=this.secuenciador.incrementAndGet();
        this.proveedor.put(p.id, p);
        return p.id;
    }

    public List<Proveedor> obtenerProveedor() {
        return this.proveedor.entrySet().stream().map(x -> x.getValue()).collect(Collectors.toList());
    }

    public Proveedor obtenerProveedor(long id) {
        final Proveedor p = this.proveedor.get(id);
        return p;
    }

    public void reemplazarProveedor (long id, Proveedor p){
        p.id = id;
         this.proveedor.put(id, p);
    }

    public void borrarProveedor (long id) {
        this.proveedor.remove(id);
    }
}
