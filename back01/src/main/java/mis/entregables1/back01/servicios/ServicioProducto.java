package mis.entregables1.back01.servicios;

import mis.entregables1.back01.modelo.Producto;
import mis.entregables1.back01.modelo.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioProducto {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final AtomicLong secuenciador2 = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Producto> productos = new ConcurrentHashMap<>();

    public long agregar(Producto p){
        p.id=this.secuenciador.incrementAndGet();
        this.productos.put(p.id, p);
        final User u=new User();
        u.id=this.secuenciador2.incrementAndGet();;
        u.nombre="ABCDEFG";
        p.idsUsers.add(u);
        return p.id;
    }

    public List<Producto> obtenerProductos() {
        return this.productos.entrySet().stream().map(x -> x.getValue()).collect(Collectors.toList());
    }

    public Producto obtenerProducto(long id) {
        final Producto p = this.productos.get(id);
        return p;
    }

    public void reemplazarProducto (long id, Producto p){
        p.id = id;
        //FIXME: verificar que exista el producto antes de reemplazar
        this.productos.put(id, p);
    }

    public void borrarProducto (long id){
        this.productos.remove(id);

    }
    public long agregarUsuario(Producto p, User u){
        p.id=this.secuenciador.incrementAndGet();
        this.productos.get(p.id).idsUsers.add(u);   //se agrega un elemento a la lista de usuario del objeto producto
        return p.id;
    }
}
