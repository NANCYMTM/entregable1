package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Producto;
import mis.entregables1.back01.servicios.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${apiv1}/productose/{idProducto}/users")
public class ControladorEntitySubRecurso {
    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public ResponseEntity getProductUsers(@PathVariable long idProducto){
        Producto x=servicioProducto.obtenerProducto(idProducto);
        if (x == null){
            return new ResponseEntity( "Producto No encontrado",HttpStatus.NOT_FOUND);
        }
        if (x.idsUsers!=null)
            return ResponseEntity.ok(x.idsUsers);
        else
            return new ResponseEntity( HttpStatus.NO_CONTENT);
        }
}