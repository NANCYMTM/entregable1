package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Producto;
import mis.entregables1.back01.servicios.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("${apiv1}/productose")
public class ControladorListaProductos {
    @Autowired
    ServicioProducto servicioProducto;

    @PostMapping
    public void agregarProducto(@RequestBody Producto p){
        this.servicioProducto.agregar(p);

    }
    @GetMapping
    public List<Producto> obtenerProductos(){
        return this.servicioProducto.obtenerProductos();
    }
}
