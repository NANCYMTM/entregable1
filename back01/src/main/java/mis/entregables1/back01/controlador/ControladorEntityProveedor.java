package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Proveedor;
import mis.entregables1.back01.servicios.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${apiv1}/proveedores/{idProveedor}")
public class ControladorEntityProveedor {
    @Autowired
    ServicioProveedor servicioProveedor;

    @GetMapping
    public ResponseEntity<Proveedor> obtenerProveedor(@PathVariable long idProveedor) {
        Proveedor p=servicioProveedor.obtenerProveedor(idProveedor);
        if (p == null){
            return new ResponseEntity( "Proveedor No encontrado", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(p);
    }

    @PostMapping
    public ResponseEntity<Proveedor> addProveedor(@RequestBody Proveedor proveedor) {
        servicioProveedor.agregar(proveedor);
        return new ResponseEntity( "Proveedor created successfully!",HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity updateProveedor(@PathVariable long idProveedor,
                                         @RequestBody Proveedor p){
        Proveedor x= servicioProveedor.obtenerProveedor(idProveedor);
        if (x == null){
            return new ResponseEntity( "Proveedor No encontrado",HttpStatus.NOT_FOUND);
        }
        servicioProveedor.reemplazarProveedor(idProveedor, p);
        return new ResponseEntity( "Product actualizado correctamente!",HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity deleteProveedor(@PathVariable long idProveedor){
        Proveedor x= servicioProveedor.obtenerProveedor(idProveedor);
        if (x == null){
            return new ResponseEntity( "Proveedor No encontrado",HttpStatus.NOT_FOUND);
        }
        servicioProveedor.borrarProveedor(idProveedor);
        return new ResponseEntity( HttpStatus.NO_CONTENT); // Stratus 204
    }

    private Proveedor buscarProveedorPorId(long idProveedor) {
        final Proveedor p = this.servicioProveedor.obtenerProveedor(idProveedor);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }

}
