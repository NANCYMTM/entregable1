package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Producto;
import mis.entregables1.back01.servicios.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${apiv1}/productose/precio/{idProducto}")
public class ControladorEntityPatch {

    @Autowired
    ServicioProducto servicioProducto;

    public static class ProductoPrecioOnly{
        public String nombre;
        public Double precio;
        public Double cantidad;
    }
    @PatchMapping
    public ResponseEntity patchPrecioProducto(@PathVariable long idProducto,
                                              @RequestBody ProductoPrecioOnly p){
        Producto x=servicioProducto.obtenerProducto(idProducto);
        if (x == null){
            return new ResponseEntity("Producto No Encontrado", HttpStatus.NOT_FOUND);
        }
        if (p.precio != null){
           if (p.precio <= 0.0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            x.precio = p.precio;
        }
        this.servicioProducto.reemplazarProducto(idProducto, x);
        return new ResponseEntity(x,HttpStatus.OK);
    }

    private Producto buscarProductoPorId(long idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
