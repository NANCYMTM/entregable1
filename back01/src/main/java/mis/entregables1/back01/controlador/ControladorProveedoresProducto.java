package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Producto;
import mis.entregables1.back01.modelo.Proveedor;
import mis.entregables1.back01.servicios.ServicioProducto;
import mis.entregables1.back01.servicios.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("${apiv1}/productose/{idProducto}/proveedores")
public class ControladorProveedoresProducto {
    @Autowired
    ServicioProducto servicioProducto;
    @Autowired
    ServicioProveedor servicioProveedor;
    // GET [] , POST
    @GetMapping
    public List<Long> obtenerIdsProveedores(@PathVariable long idProducto){
        final Producto p = obtenerProductoPorId(idProducto);
        return p.idsProveedores;
    }

    public static class ProveedorEntrada{
        public long idProveedor;
    }

    @PutMapping
    public void agregarReferenciaProveedorProducto(@PathVariable long idProducto,
                                                   @RequestBody ProveedorEntrada p){
        final Producto x = obtenerProductoPorId(idProducto);
        final Proveedor v = this.servicioProveedor.obtenerProveedor(p.idProveedor);
        if (v==null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        for (long id: x.idsProveedores){
            if(id == p.idProveedor)
                return;
        }
        x.idsProveedores.add(p.idProveedor);
        this.servicioProducto.reemplazarProducto(idProducto, x);


    }
    public Producto obtenerProductoPorId(long idProducto){
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if (p==null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }


}
