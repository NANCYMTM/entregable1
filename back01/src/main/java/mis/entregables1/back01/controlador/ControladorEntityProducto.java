package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Producto;
import mis.entregables1.back01.servicios.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@RestController
@RequestMapping("${apiv1}/productose/{idProducto}")
public class ControladorEntityProducto {
    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public ResponseEntity<Producto> obtenerProducto(@PathVariable long idProducto) {
        Producto p= servicioProducto.obtenerProducto(idProducto);
        if (p == null){
            return new ResponseEntity( "Producto No encontrado", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(p);
    }

    @PostMapping
    public ResponseEntity<Producto> addProducto(@RequestBody Producto producto) {
        servicioProducto.agregar(producto);
        return new ResponseEntity( "Product created successfully!",HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity updateProducto(@PathVariable long idProducto,
                                         @RequestBody Producto p){
        Producto x= servicioProducto.obtenerProducto(idProducto);
        if (x == null){
            return new ResponseEntity( "Producto No encontrado",HttpStatus.NOT_FOUND);
        }
        servicioProducto.reemplazarProducto(idProducto, p);
        return new ResponseEntity( "Product actualizado correctamente!",HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity deleteProducto(@PathVariable long idProducto){
        Producto x= servicioProducto.obtenerProducto(idProducto);
        if (x == null){
            return new ResponseEntity( "Producto No encontrado",HttpStatus.NOT_FOUND);
        }
        servicioProducto.borrarProducto(idProducto);
        return new ResponseEntity( HttpStatus.NO_CONTENT); // Stratus 204
    }
    public static class ProductoEntrada{
        public String nombre;
        public Double precio;
        public Double cantidad;
    }

    @PatchMapping
    public void modificarProducto(@PathVariable long idProducto,
                                  @RequestBody ProductoEntrada p) {
//                                  @RequestBody Producto p) {
        final Producto x = buscarProductoPorId(idProducto);
        if (p.nombre != null) {
            if (p.nombre.trim().isEmpty())
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.nombre = p.nombre;
        }
        if (p.precio != null) {
            if (p.precio <= 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.precio = p.precio;
        }
        if (p.cantidad != null) {
            if (p.cantidad < 0.0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            x.cantidad = p.cantidad;
        }
        this.servicioProducto.reemplazarProducto(idProducto, x);

    }


    private Producto buscarProductoPorId(long idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
