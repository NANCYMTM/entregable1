package mis.entregables1.back01.controlador;

import mis.entregables1.back01.modelo.Proveedor;
import mis.entregables1.back01.servicios.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${apiv1}/proveedores")
public class ControladorListaProveedores {
    @Autowired
    ServicioProveedor servicioProveedor;

    @PostMapping
    public void agregarProveedor(@RequestBody Proveedor p){
        this.servicioProveedor.agregar(p);

    }
    @GetMapping
    public List<Proveedor> obtenerProveedor(){
        return this.servicioProveedor.obtenerProveedor();
    }
}
