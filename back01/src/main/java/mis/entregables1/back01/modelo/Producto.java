package mis.entregables1.back01.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;


public class Producto {
    public long id;
    public String nombre;
    public double precio;
    public double cantidad;

    public final List<Long>idsProveedores = new ArrayList<>();
    public final List<User>idsUsers = new ArrayList<>();
}
